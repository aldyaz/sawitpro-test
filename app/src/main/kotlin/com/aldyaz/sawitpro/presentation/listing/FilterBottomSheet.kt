@file:OptIn(ExperimentalMaterial3Api::class)

package com.aldyaz.sawitpro.presentation.listing

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material3.Button
import androidx.compose.material3.DatePicker
import androidx.compose.material3.DatePickerDialog
import androidx.compose.material3.DateRangePicker
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.ModalBottomSheet
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.material3.rememberDatePickerState
import androidx.compose.material3.rememberModalBottomSheetState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableLongStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import com.aldyaz.sawitpro.presentation.model.FilterTypeViewEntity

@Composable
fun FilterBottomSheetDialog(
    filterState: MutableState<FilterTypeViewEntity>,
    onDismissRequest: () -> Unit,
    onSubmitFilter: (FilterTypeViewEntity) -> Unit
) {
    val bottomSheetState = rememberModalBottomSheetState(
        skipPartiallyExpanded = true
    )

    ModalBottomSheet(
        onDismissRequest = onDismissRequest,
        sheetState = bottomSheetState,
        containerColor = Color.White
    ) {
        FilterDialogContent(
            filterState = filterState,
            onSubmitFilter = onSubmitFilter
        )
    }
}

@Composable
fun FilterDialogContent(
    filterState: MutableState<FilterTypeViewEntity>,
    onSubmitFilter: (FilterTypeViewEntity) -> Unit
) {
    var selectedDate by remember {
        mutableLongStateOf(0L)
    }
    var filter by filterState
    var showDatePicker by remember {
        mutableStateOf(false)
    }
    val datePickerState = rememberDatePickerState()

    if (showDatePicker) {
        DatePickerDialog(
            onDismissRequest = {
                showDatePicker = false
            },
            confirmButton = {
                Button(
                    onClick = {
                        showDatePicker = false
                        selectedDate =
                            datePickerState.selectedDateMillis ?: System.currentTimeMillis()
                    }
                ) {
                    Text(text = "Select")
                }
            }
        ) {
            DatePicker(state = datePickerState)
        }
    }

    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(Color.White)
            .padding(16.dp)
    ) {

        Row(
            modifier = Modifier.fillMaxWidth()
        ) {
            Column(
                modifier = Modifier.weight(1f)
            ) {
                Text(text = "Mulai dari")
                OutlinedTextField(
                    value = if (filter.fromDate > 0L) filter.fromDate.toString() else "",
                    onValueChange = { newValue ->
                        filter = filter.copy(
                            fromDate = newValue.toLong()
                        )
                    },
                    enabled = false,
                    modifier = Modifier.clickable {
                        showDatePicker = true
                    }
                )
            }

            Spacer(modifier = Modifier.width(8.dp))

            Column(
                modifier = Modifier.weight(1f)
            ) {
                Text(text = "Sampai")
                OutlinedTextField(
                    value = if (filter.toDate > 0L) filter.toDate.toString() else "",
                    onValueChange = { newValue ->
                        filter = filter.copy(
                            toDate = newValue.toLong()
                        )
                    },
                    enabled = false,
                    modifier = Modifier.clickable {
                        showDatePicker = true
                    }
                )
            }
        }

        Spacer(modifier = Modifier.height(8.dp))

        Text(text = "Nama Driver")
        OutlinedTextField(
            value = filter.driverName,
            onValueChange = { newValue ->
                filter = filter.copy(
                    driverName = newValue
                )
            },
            modifier = Modifier.fillMaxWidth()
        )

        Spacer(modifier = Modifier.height(8.dp))

        Text(text = "Plat Nomor")
        OutlinedTextField(
            value = filter.licenseNumber,
            onValueChange = { newValue ->
                filter = filter.copy(
                    licenseNumber = newValue
                )
            },
            modifier = Modifier.fillMaxWidth()
        )

        Spacer(modifier = Modifier.height(8.dp))

        Row(
            modifier = Modifier.fillMaxWidth()
        ) {
            Button(
                onClick = {
                    filter = filter.copy(
                        fromDate = 0L,
                        toDate = 0L,
                        driverName = "",
                        licenseNumber = ""
                    )
                },
                modifier = Modifier.weight(1f)
            ) {
                Text(text = "Clear")
            }

            Spacer(modifier = Modifier.width(8.dp))

            Button(
                onClick = {
                    onSubmitFilter(filter)
                },
                modifier = Modifier.weight(1f)
            ) {
                Text(text = "Apply")
            }
        }
    }
}
