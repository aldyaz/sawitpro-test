package com.aldyaz.sawitpro.presentation.listing

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.aldyaz.sawitpro.base.BaseViewModel
import com.aldyaz.sawitpro.base.DispatcherProvider
import com.aldyaz.sawitpro.base.ResultState
import com.aldyaz.sawitpro.domain.interactor.GetTickets
import com.aldyaz.sawitpro.domain.model.FilterType
import com.aldyaz.sawitpro.domain.model.SortType
import com.aldyaz.sawitpro.presentation.model.FilterTypeViewEntity
import com.aldyaz.sawitpro.presentation.model.SortingTypeViewEntity
import com.aldyaz.sawitpro.presentation.util.mapToUiModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class TicketListViewModel @Inject constructor(
    private val getTickets: GetTickets,
    private val dispatcherProvider: DispatcherProvider
) : BaseViewModel<TicketListIntent>() {

    private val _state = MutableLiveData<TicketListState>().apply {
        value = TicketListState.initial()
    }
    val state: LiveData<TicketListState>
        get() = _state

    override fun onDispatchIntent(intent: TicketListIntent) {
        when (intent) {
            is TicketListIntent.Fetch -> fetchTicketList(
                intent.sortingTypeViewEntity,
                intent.filterType
            )
        }
    }

    private fun fetchTicketList(
        sortingTypeViewEntity: SortingTypeViewEntity,
        filterType: FilterTypeViewEntity?
    ) = viewModelScope.launch(dispatcherProvider.io) {
        _state.postUpdateState {
            copy(
                isLoading = true
            )
        }
        when (val result = getTickets(
            sortType = sortingTypeViewEntity.toDomainModel(),
            filterType = filterType?.toDomainModel()
        )) {
            is ResultState.Error -> {
                _state.postUpdateState {
                    copy(
                        isLoading = false,
                        errorMessage = result.throwable.message ?: "An Error Occured!"
                    )
                }
            }

            is ResultState.Success -> {
                _state.postUpdateState {
                    copy(
                        isLoading = false,
                        tickets = result.data.map {
                            it.mapToUiModel()
                        }
                    )
                }
            }
        }
    }

    private fun FilterTypeViewEntity.toDomainModel(): FilterType {
        return FilterType(
            fromDate = fromDate,
            toDate = toDate,
            driverName = driverName,
            licenseNumber = licenseNumber
        )
    }

    private fun SortingTypeViewEntity.toDomainModel(): SortType {
        return when (this) {
            SortingTypeViewEntity.DATE_TIME -> SortType.DATE_TIME
            SortingTypeViewEntity.DRIVER_NAME -> SortType.DRIVER_NAME
            SortingTypeViewEntity.LICENSE_NUMBER -> SortType.LICENSE_NUMBER
        }
    }
}