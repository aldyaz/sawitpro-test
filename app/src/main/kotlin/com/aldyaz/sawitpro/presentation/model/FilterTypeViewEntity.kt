package com.aldyaz.sawitpro.presentation.model

data class FilterTypeViewEntity(
    val fromDate: Long = 0L,
    val toDate: Long = 0L,
    val driverName: String = "",
    val licenseNumber: String = ""
)