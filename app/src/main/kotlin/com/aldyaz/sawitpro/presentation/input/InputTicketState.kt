package com.aldyaz.sawitpro.presentation.input

import com.aldyaz.sawitpro.presentation.model.TicketViewEntity

data class InputTicketState(
    val ticket: TicketViewEntity = TicketViewEntity(),
    val netWeightText: String = "",
    val newNetWeight: Int? = null,
    val globalErrorMessage: String = "",
    val showLoadingProgress: Boolean = false
) {
    companion object {
        fun initial(): InputTicketState = InputTicketState()
    }
}