package com.aldyaz.sawitpro.presentation.model

enum class SortingTypeViewEntity(
    val text: String
) {
    DATE_TIME("Date Time"),
    DRIVER_NAME("Driver Name"),
    LICENSE_NUMBER("License Number")
}