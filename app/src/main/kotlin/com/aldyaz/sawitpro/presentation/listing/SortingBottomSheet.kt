@file:OptIn(ExperimentalMaterial3Api::class)

package com.aldyaz.sawitpro.presentation.listing

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Check
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.ModalBottomSheet
import androidx.compose.material3.Text
import androidx.compose.material3.rememberModalBottomSheetState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.aldyaz.sawitpro.presentation.model.SortingTypeViewEntity

@Composable
fun SortingBottomSheet(
    sortState: MutableState<SortingTypeViewEntity>,
    onDismissRequest: () -> Unit,
    onSelectSortType: () -> Unit
) {
    val bottomSheetState = rememberModalBottomSheetState(
        skipPartiallyExpanded = true
    )

    ModalBottomSheet(
        onDismissRequest = onDismissRequest,
        sheetState = bottomSheetState,
        containerColor = Color.White
    ) {
        SortingBottomSheetContent(
            sortState = sortState,
            onSelectSortType = onSelectSortType
        )
    }
}

@Composable
fun SortingBottomSheetContent(
    sortState: MutableState<SortingTypeViewEntity>,
    onSelectSortType: () -> Unit
) {
    var sortType by sortState

    LazyColumn(
        modifier = Modifier.fillMaxWidth()
    ) {
        itemsIndexed(SortingTypeViewEntity.values()) { _, item ->
            SortingItem(
                sortType = item,
                selected = sortType == item
            ) {
                sortType = item
                onSelectSortType()
            }
        }
    }
}

@Composable
fun SortingItem(
    sortType: SortingTypeViewEntity,
    selected: Boolean,
    onSelect: () -> Unit
) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(16.dp)
            .clickable {
                onSelect()
            },
        verticalAlignment = Alignment.CenterVertically
    ) {
        Text(
            text = sortType.text,
            modifier = Modifier.weight(1f)
        )
        if (selected) {
            Image(
                imageVector = Icons.Default.Check,
                contentDescription = "Selected sort type"
            )
        }
    }
}

@Preview
@Composable
fun SortingItemPreview() {
    SortingItem(
        sortType = SortingTypeViewEntity.LICENSE_NUMBER,
        selected = false,
        onSelect = {}
    )
}
