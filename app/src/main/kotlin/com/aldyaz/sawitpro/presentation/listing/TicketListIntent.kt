package com.aldyaz.sawitpro.presentation.listing

import com.aldyaz.sawitpro.presentation.model.FilterTypeViewEntity
import com.aldyaz.sawitpro.presentation.model.SortingTypeViewEntity

sealed class TicketListIntent {

    data class Fetch(
        val sortingTypeViewEntity: SortingTypeViewEntity = SortingTypeViewEntity.DATE_TIME,
        val filterType: FilterTypeViewEntity? = null
    ) : TicketListIntent()

}