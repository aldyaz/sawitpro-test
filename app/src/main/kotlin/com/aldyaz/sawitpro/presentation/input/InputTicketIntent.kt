package com.aldyaz.sawitpro.presentation.input

import com.aldyaz.sawitpro.presentation.model.TicketViewEntity

sealed class InputTicketIntent {

    data class FetchTicket(
        val id: String
    ) : InputTicketIntent()

    data class OnSubmitTicket(
        val ticket: TicketViewEntity
    ) : InputTicketIntent()

    data class OnInputInbound(
        val value: Int? = 0
    ) : InputTicketIntent()

    data class OnInputOutbound(
        val value: Int? = 0
    ) : InputTicketIntent()

}