@file:OptIn(ExperimentalMaterial3Api::class)

package com.aldyaz.sawitpro.presentation.input

import android.widget.Toast
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Check
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.aldyaz.sawitpro.base.component.FormField
import com.aldyaz.sawitpro.base.component.LoadingProgressDialog
import com.aldyaz.sawitpro.presentation.model.TicketViewEntity

@Composable
fun InputTicket(
    ticketId: String?,
    onSuccessSubmitTicket: () -> Unit
) {
    val viewModel: InputTicketViewModel = hiltViewModel()
    val state by viewModel.state.observeAsState()
    val submission by viewModel.submission.observeAsState()
    val context = LocalContext.current

    ticketId?.let {
        LaunchedEffect(Unit) {
            viewModel.onDispatchIntent(InputTicketIntent.FetchTicket(it))
        }
    }

    submission?.let {
        Toast.makeText(
            context,
            if (ticketId != null) {
                "Success update ticket"
            } else "Success add new ticket",
            Toast.LENGTH_SHORT
        ).show()
        onSuccessSubmitTicket()
    }

    state?.let {
        InputTicketContent(
            state = it,
            onInboundChange = { inbound ->
                viewModel.onDispatchIntent(InputTicketIntent.OnInputInbound(inbound))
            },
            onOutboundChange = { outbound ->
                viewModel.onDispatchIntent(InputTicketIntent.OnInputOutbound(outbound))
            },
            onClickSubmit = { ticket ->
                viewModel.onDispatchIntent(InputTicketIntent.OnSubmitTicket(ticket))
            }
        )
    }
}

@Composable
fun InputTicketContent(
    state: InputTicketState,
    onInboundChange: (Int?) -> Unit,
    onOutboundChange: (Int?) -> Unit,
    onClickSubmit: (TicketViewEntity) -> Unit
) {
    val ticket = state.ticket

    var driverName by remember {
        mutableStateOf(ticket.driverName)
    }
    var licenseNumber by remember {
        mutableStateOf(ticket.licenseNumber)
    }
    var inboundWeight by remember {
        mutableStateOf(ticket.inboundWeight)
    }
    var outboundWeight by remember {
        mutableStateOf(ticket.outboundWeight)
    }
    val netWeightText = if (state.newNetWeight != null && state.newNetWeight > 0) {
        state.newNetWeight.toString()
    } else state.netWeightText
    var showLoading by remember { mutableStateOf(state.showLoadingProgress) }

    LaunchedEffect(ticket) {
        driverName = ticket.driverName
        licenseNumber = ticket.licenseNumber
        inboundWeight = ticket.inboundWeight
        outboundWeight = ticket.outboundWeight
        showLoading = state.showLoadingProgress
    }

    if (showLoading) {
        LoadingProgressDialog()
    }

    Scaffold(
        topBar = {
            TopAppBar(
                title = {
                    Text(text = "Input Ticket")
                },
                actions = {
                    IconButton(
                        enabled = ((inboundWeight ?: 0) > (outboundWeight ?: 0)),
                        onClick = {
                            onClickSubmit(
                                ticket.copy(
                                    id = ticket.id,
                                    dateTime = if (ticket.dateTime == 0L) System.currentTimeMillis()
                                    else ticket.dateTime,
                                    driverName = driverName,
                                    licenseNumber = licenseNumber,
                                    inboundWeight = inboundWeight,
                                    outboundWeight = outboundWeight,
                                    netWeight = state.newNetWeight ?: 0
                                )
                            )
                        }
                    ) {
                        Icon(
                            imageVector = Icons.Default.Check,
                            contentDescription = "Submit Ticket"
                        )
                    }
                }
            )
        }
    ) { contentPadding ->
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(contentPadding)
        ) {
            FormField(
                value = driverName,
                onTextChange = {
                    driverName = it
                },
                placeholderText = "Driver Name",
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(
                        horizontal = 16.dp,
                        vertical = 8.dp
                    )
            )
            FormField(
                value = licenseNumber,
                onTextChange = {
                    licenseNumber = it
                },
                placeholderText = "License Number",
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(
                        horizontal = 16.dp,
                        vertical = 8.dp
                    )
            )
            FormField(
                value = inboundWeight?.let { "$it" }.orEmpty(),
                onTextChange = { inbound ->
                    inboundWeight = if (inbound.isNotEmpty()) {
                        inbound.toInt()
                    } else {
                        null
                    }
                    onInboundChange(inboundWeight)
                },
                placeholderText = "Inbound Weight",
                keyboardOptions = KeyboardOptions(
                    keyboardType = KeyboardType.Number,
                    imeAction = ImeAction.Next
                ),
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(
                        horizontal = 16.dp,
                        vertical = 8.dp
                    )
            )
            FormField(
                value = outboundWeight?.let { "$it" }.orEmpty(),
                onTextChange = { outbound ->
                    outboundWeight = if (outbound.isNotEmpty()) {
                        outbound.toInt()
                    } else {
                        null
                    }
                    onOutboundChange(outboundWeight)
                },
                placeholderText = "Outbound Weight",
                keyboardOptions = KeyboardOptions(
                    keyboardType = KeyboardType.Number,
                    imeAction = ImeAction.Done
                ),
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(
                        horizontal = 16.dp,
                        vertical = 8.dp
                    )
            )
            Text(
                text = "Net Weight: $netWeightText",
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(16.dp)
            )
        }
    }
}
