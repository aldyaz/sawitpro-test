package com.aldyaz.sawitpro.presentation.util

import com.aldyaz.sawitpro.domain.model.Ticket
import com.aldyaz.sawitpro.presentation.model.TicketViewEntity

fun Ticket.mapToUiModel(): TicketViewEntity = TicketViewEntity(
    id = id,
    dateTime = dateTime,
    driverName = driverName,
    licenseNumber = licenseNumber,
    inboundWeight = inboundWeight,
    outboundWeight = outboundWeight,
    netWeight = netWeight
)

fun TicketViewEntity.mapToDomainModel(): Ticket = Ticket(
    id = id,
    dateTime = dateTime,
    driverName = driverName,
    licenseNumber = licenseNumber,
    inboundWeight = inboundWeight ?: 0,
    outboundWeight = outboundWeight ?: 0,
    netWeight = netWeight
)