package com.aldyaz.sawitpro.presentation.model

data class TicketViewEntity(
    val id: String = "",
    val dateTime: Long = 0L,
    val licenseNumber: String = "",
    val driverName: String = "",
    val inboundWeight: Int? = null,
    val outboundWeight: Int? = null,
    val netWeight: Int = 0
)