@file:OptIn(ExperimentalMaterial3Api::class)

package com.aldyaz.sawitpro.presentation.listing

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.ArrowDropDown
import androidx.compose.material3.Button
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import androidx.hilt.navigation.compose.hiltViewModel
import com.aldyaz.sawitpro.R
import com.aldyaz.sawitpro.base.component.FullError
import com.aldyaz.sawitpro.base.component.FullLoading
import com.aldyaz.sawitpro.presentation.model.FilterTypeViewEntity
import com.aldyaz.sawitpro.presentation.model.SortingTypeViewEntity
import com.aldyaz.sawitpro.presentation.model.TicketViewEntity

@Composable
fun TicketList(
    onClickAddTicket: () -> Unit,
    onClickEditTicket: (String) -> Unit
) {
    val viewModel: TicketListViewModel = hiltViewModel()
    val state by viewModel.state.observeAsState()
    val sortState = remember {
        mutableStateOf(SortingTypeViewEntity.DATE_TIME)
    }
    val filterState = remember {
        mutableStateOf(FilterTypeViewEntity())
    }

    LaunchedEffect(Unit) {
        viewModel.onDispatchIntent(
            TicketListIntent.Fetch(
                sortState.value,
                filterState.value
            )
        )
    }

    state?.let {
        TicketListContent(
            state = it,
            sortState = sortState,
            filterState = filterState,
            onRetry = {
                viewModel.onDispatchIntent(
                    TicketListIntent.Fetch(
                        sortState.value,
                        filterState.value
                    )
                )
            },
            onClickAddTicket = onClickAddTicket,
            onClickEditTicket = onClickEditTicket,
            onSubmitFilter = { filter ->
                viewModel.onDispatchIntent(
                    TicketListIntent.Fetch(
                        sortState.value,
                        filter
                    )
                )
            },
            onSelectSortType = {
                viewModel.onDispatchIntent(
                    TicketListIntent.Fetch(
                        sortState.value,
                        filterState.value
                    )
                )
            }
        )
    }
}

@Composable
fun TicketListContent(
    state: TicketListState,
    sortState: MutableState<SortingTypeViewEntity>,
    filterState: MutableState<FilterTypeViewEntity>,
    onRetry: () -> Unit,
    onClickAddTicket: () -> Unit,
    onClickEditTicket: (String) -> Unit,
    onSubmitFilter: (FilterTypeViewEntity) -> Unit,
    onSelectSortType: () -> Unit
) {
    var ticketDetail: TicketViewEntity? by remember {
        mutableStateOf(null)
    }
    var showDetailDialog by remember {
        mutableStateOf(false)
    }
    var showSortingDialog by remember {
        mutableStateOf(false)
    }
    var showFilterDialog by remember {
        mutableStateOf(false)
    }

    if (showDetailDialog) {
        ticketDetail?.let {
            TicketDetailDialog(
                ticket = it,
                onClickEditTicket = { ticketId ->
                    onClickEditTicket(ticketId)
                    showDetailDialog = false
                },
                onDismissRequest = {
                    showDetailDialog = false
                }
            )
        }
    }

    if (showSortingDialog) {
        SortingBottomSheet(
            sortState = sortState,
            onDismissRequest = {
                showSortingDialog = false
            },
            onSelectSortType = onSelectSortType
        )
    }

    if (showFilterDialog) {
        FilterBottomSheetDialog(
            filterState = filterState,
            onDismissRequest = {
                showFilterDialog = false
            },
            onSubmitFilter = {
                onSubmitFilter(it)
                showFilterDialog = false
            }
        )
    }

    Scaffold(
        topBar = {
            TopAppBar(
                title = {
                    Text(text = "Weighbridge")
                },
                actions = {
                    IconButton(onClick = {
                        showSortingDialog = true
                    }) {
                        Icon(
                            painter = painterResource(R.drawable.ic_filter_list),
                            contentDescription = "Filter Ticket List"
                        )
                    }
                    IconButton(onClick = {
                        showFilterDialog = true
                    }) {
                        Icon(
                            imageVector = Icons.Default.ArrowDropDown,
                            contentDescription = "Filter Ticket List"
                        )
                    }
                    IconButton(onClick = onClickAddTicket) {
                        Icon(
                            imageVector = Icons.Default.Add,
                            contentDescription = "Add New Ticket"
                        )
                    }
                }
            )
        }
    ) { contentPadding ->
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .padding(contentPadding)
        ) {
            when {
                state.isLoading -> FullLoading()
                state.errorMessage.isNotEmpty() -> FullError(
                    onRetryClick = onRetry
                )

                else -> Content(
                    items = state.tickets,
                    onClickTicket = {
                        ticketDetail = it
                        showDetailDialog = true
                    }
                )
            }
        }
    }
}

@Composable
private fun Content(
    items: List<TicketViewEntity>,
    onClickTicket: (TicketViewEntity) -> Unit,
    modifier: Modifier = Modifier
) {
    LazyColumn(
        modifier = modifier,
        contentPadding = PaddingValues(16.dp),
        verticalArrangement = Arrangement.spacedBy(8.dp)
    ) {
        itemsIndexed(items) { _, item ->
            TicketItem(
                ticket = item,
                onClickTicket = onClickTicket
            )
        }
    }
}

@Composable
private fun TicketItem(
    ticket: TicketViewEntity,
    onClickTicket: (TicketViewEntity) -> Unit,
    modifier: Modifier = Modifier
) {
    Card(
        modifier = modifier
            .fillMaxWidth()
            .clickable {
                onClickTicket(ticket)
            },
        border = BorderStroke(1.dp, Color.Black),
        colors = CardDefaults.cardColors(
            containerColor = Color.White
        ),
        shape = RoundedCornerShape(8.dp)
    ) {
        Column(
            modifier = Modifier.padding(16.dp)
        ) {
            Text(text = "ID: ${ticket.id}")
            Spacer(modifier = Modifier.height(4.dp))
            Text(text = "Driver Name: ${ticket.driverName}")
            Spacer(modifier = Modifier.height(4.dp))
            Text(text = "Date: ${ticket.dateTime}")
        }
    }
}

@Composable
fun TicketDetailDialog(
    ticket: TicketViewEntity,
    onClickEditTicket: (String) -> Unit,
    onDismissRequest: () -> Unit
) {
    Dialog(
        onDismissRequest = onDismissRequest
    ) {
        Column(
            modifier = Modifier
                .background(Color.White, RoundedCornerShape(8.dp))
                .padding(16.dp)
        ) {
            Text(text = "ID: ${ticket.id}")
            Spacer(modifier = Modifier.height(4.dp))
            Text(text = "Driver Name: ${ticket.driverName}")
            Spacer(modifier = Modifier.height(4.dp))
            Text(text = "Date: ${ticket.dateTime}")
            Spacer(modifier = Modifier.height(4.dp))
            Text(text = "License Number: ${ticket.licenseNumber}")
            Spacer(modifier = Modifier.height(4.dp))
            Text(text = "Inbound Weight: ${ticket.inboundWeight}")
            Spacer(modifier = Modifier.height(4.dp))
            Text(text = "Inbound Weight: ${ticket.outboundWeight}")
            Spacer(modifier = Modifier.height(4.dp))
            Button(
                onClick = { onClickEditTicket(ticket.id) },
                modifier = Modifier.align(Alignment.End)
            ) {
                Text(text = "Edit")
            }
        }
    }
}

@Preview
@Composable
private fun PreviewTicketItem() {
    TicketItem(
        ticket = TicketViewEntity(
            id = "asdas3232ad323123as232312313",
            dateTime = 12345678,
            licenseNumber = "B 6273 XSX",
            driverName = "Dudung",
            inboundWeight = 23,
            outboundWeight = 23,
            netWeight = 123
        ),
        onClickTicket = {}
    )
}
