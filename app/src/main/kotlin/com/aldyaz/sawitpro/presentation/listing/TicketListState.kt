package com.aldyaz.sawitpro.presentation.listing

import com.aldyaz.sawitpro.presentation.model.TicketViewEntity

data class TicketListState(
    val isLoading: Boolean = false,
    val errorMessage: String = "",
    val tickets: List<TicketViewEntity> = listOf()
) {
    companion object {
        fun initial() = TicketListState()
    }
}