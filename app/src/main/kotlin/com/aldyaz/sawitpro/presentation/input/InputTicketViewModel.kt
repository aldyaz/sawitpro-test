package com.aldyaz.sawitpro.presentation.input

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.aldyaz.sawitpro.base.BaseViewModel
import com.aldyaz.sawitpro.base.DispatcherProvider
import com.aldyaz.sawitpro.base.ResultState
import com.aldyaz.sawitpro.domain.interactor.CalculateInboundOutbound
import com.aldyaz.sawitpro.domain.interactor.GetTicket
import com.aldyaz.sawitpro.domain.interactor.InsertTicket
import com.aldyaz.sawitpro.domain.model.Ticket
import com.aldyaz.sawitpro.extension.orElse
import com.aldyaz.sawitpro.presentation.model.TicketViewEntity
import com.aldyaz.sawitpro.presentation.util.mapToDomainModel
import com.aldyaz.sawitpro.presentation.util.mapToUiModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class InputTicketViewModel @Inject constructor(
    private val insertTicket: InsertTicket,
    private val getTicket: GetTicket,
    private val calculateInboundOutbound: CalculateInboundOutbound,
    private val dispatcherProvider: DispatcherProvider
) : BaseViewModel<InputTicketIntent>() {

    private val _state = MutableLiveData<InputTicketState>().apply {
        value = InputTicketState.initial()
    }
    val state: LiveData<InputTicketState>
        get() = _state

    private val _submission = MutableLiveData<Unit>()
    val submission: LiveData<Unit>
        get() = _submission

    private var _inboundWeight: Int? by mutableStateOf(null)
    private var _outboundWeight: Int? by mutableStateOf(null)

    override fun onDispatchIntent(intent: InputTicketIntent) {
        when (intent) {
            is InputTicketIntent.FetchTicket -> fetchTicket(intent.id)
            is InputTicketIntent.OnInputInbound -> {
                _inboundWeight = intent.value
                calculateWeight()
            }

            is InputTicketIntent.OnInputOutbound -> {
                _outboundWeight = intent.value
                calculateWeight()
            }

            is InputTicketIntent.OnSubmitTicket -> submitTicket(intent.ticket)
        }
    }

    private fun fetchTicket(id: String) = viewModelScope.launch(dispatcherProvider.io) {
        _state.postUpdateState {
            copy(
                showLoadingProgress = true
            )
        }
        when (val result = getTicket(id)) {
            is ResultState.Error -> {
                _state.postUpdateState {
                    copy(
                        showLoadingProgress = false,
                        globalErrorMessage = result.throwable.message.orElse("On Error Occured!")
                    )
                }
            }

            is ResultState.Success -> {
                val data = result.data.mapToUiModel()
                _state.postUpdateState {
                    copy(
                        showLoadingProgress = false,
                        ticket = data,
                        newNetWeight = data.netWeight
                    )
                }
                _inboundWeight = data.inboundWeight
                _outboundWeight = data.outboundWeight

                calculateWeight()
            }
        }
    }

    private fun calculateWeight() = viewModelScope.launch(dispatcherProvider.io) {
        when (val result = calculateInboundOutbound(
            inboundWeight = _inboundWeight ?: 0,
            outboundWeight = _outboundWeight ?: 0
        )) {
            is ResultState.Error -> {
                _state.postUpdateState {
                    copy(
                        newNetWeight = 0,
                        netWeightText = result.throwable.message.orEmpty()
                    )
                }
            }

            is ResultState.Success -> {
                val netWeight = result.data
                _state.postUpdateState {
                    copy(
                        newNetWeight = netWeight,
                        netWeightText = newNetWeight.toString()
                    )
                }
            }
        }
    }

    private fun submitTicket(
        ticket: TicketViewEntity
    ) = viewModelScope.launch(dispatcherProvider.io) {
        _state.postUpdateState {
            copy(
                showLoadingProgress = true
            )
        }
        when (val result = insertTicket(ticket.mapToDomainModel())) {
            is ResultState.Error -> {
                _state.postUpdateState {
                    copy(
                        showLoadingProgress = false,
                        globalErrorMessage = result.throwable.message.orElse(
                            "On Error Occured!"
                        )
                    )
                }
            }

            is ResultState.Success -> {
                _state.postUpdateState {
                    copy(
                        showLoadingProgress = false
                    )
                }
                _submission.postValue(Unit)
            }
        }
    }
}