package com.aldyaz.sawitpro.di

import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.Logger
import com.google.firebase.database.ktx.database
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class FirebaseModule {

    @Singleton
    @Provides
    fun provideRealtimeDatabase(): FirebaseDatabase = Firebase.database.apply {
        setLogLevel(Logger.Level.DEBUG)
    }

    @Singleton
    @Provides
    fun provideCloudFirestore(): FirebaseFirestore = Firebase.firestore
}