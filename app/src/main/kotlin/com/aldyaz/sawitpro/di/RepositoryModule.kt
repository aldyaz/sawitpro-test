package com.aldyaz.sawitpro.di

import com.aldyaz.sawitpro.data.repository.FirestoreRepository
import com.aldyaz.sawitpro.data.repository.FirestoreRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@InstallIn(SingletonComponent::class)
@Module
abstract class RepositoryModule {

    @Binds
    abstract fun bindFirestoreRepository(
        impl: FirestoreRepositoryImpl
    ): FirestoreRepository

}