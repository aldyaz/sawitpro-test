package com.aldyaz.sawitpro.di

import com.aldyaz.sawitpro.domain.interactor.CalculateInboundOutbound
import com.aldyaz.sawitpro.domain.interactor.CalculateInboundOutboundImpl
import com.aldyaz.sawitpro.domain.interactor.GetTicket
import com.aldyaz.sawitpro.domain.interactor.GetTicketImpl
import com.aldyaz.sawitpro.domain.interactor.GetTickets
import com.aldyaz.sawitpro.domain.interactor.GetTicketsImpl
import com.aldyaz.sawitpro.domain.interactor.InsertTicket
import com.aldyaz.sawitpro.domain.interactor.InsertTicketImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@InstallIn(SingletonComponent::class)
@Module
abstract class InteractorModule {

    @Binds
    abstract fun bindGetTickets(
        impl: GetTicketsImpl
    ): GetTickets

    @Binds
    abstract fun bindGetTicket(
        impl: GetTicketImpl
    ): GetTicket

    @Binds
    abstract fun bindInsertTicket(
        impl: InsertTicketImpl
    ): InsertTicket

    @Binds
    abstract fun bindCalculateInboundOutbound(
        impl: CalculateInboundOutboundImpl
    ): CalculateInboundOutbound

}