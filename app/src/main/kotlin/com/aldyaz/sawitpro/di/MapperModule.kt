package com.aldyaz.sawitpro.di

import com.aldyaz.sawitpro.domain.mapper.TicketDataToDomainMapper
import com.aldyaz.sawitpro.domain.mapper.TicketDomainToDataMapper
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@InstallIn(SingletonComponent::class)
@Module
class MapperModule {

    @Provides
    fun provideTicketDataToDomainMapper(): TicketDataToDomainMapper = TicketDataToDomainMapper()

    @Provides
    fun provideTicketDomainToDataMapper(): TicketDomainToDataMapper = TicketDomainToDataMapper()

}