package com.aldyaz.sawitpro.di

import com.aldyaz.sawitpro.base.DefaultDispatcherProvider
import com.aldyaz.sawitpro.base.DispatcherProvider
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class BaseModule {

    @Singleton
    @Provides
    fun provideGson(): Gson = Gson()

    @Singleton
    @Provides
    fun provideDefaultDispatcherModule(): DispatcherProvider = DefaultDispatcherProvider

}