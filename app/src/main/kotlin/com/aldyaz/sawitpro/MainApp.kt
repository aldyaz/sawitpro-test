package com.aldyaz.sawitpro

import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.aldyaz.sawitpro.base.Route
import com.aldyaz.sawitpro.base.Screen
import com.aldyaz.sawitpro.presentation.input.InputTicket
import com.aldyaz.sawitpro.presentation.listing.TicketList
import com.aldyaz.sawitpro.ui.theme.AppTheme

@Composable
fun MainApp() {
    AppTheme {
        AppNavigation()
    }
}

@Composable
fun AppNavigation(
    navHostController: NavHostController = rememberNavController()
) {
    NavHost(
        navController = navHostController,
        startDestination = Route.TICKET_LIST
    ) {
        composable(Route.TICKET_LIST) {
            TicketList(
                onClickAddTicket = {
                    navHostController.navigate(
                        Screen.InputTicket.pattern()
                    )
                },
                onClickEditTicket = {
                    navHostController.navigate(
                        Screen.InputTicket.pattern(it)
                    )
                }
            )
        }

        composable(
            Route.INPUT_TICKET,
            arguments = listOf(
                navArgument("id") {
                    type = NavType.StringType
                }
            )
        ) {
            val id = it.arguments?.getString("id")
            InputTicket(
                ticketId = id,
                onSuccessSubmitTicket = {
                    navHostController.navigateUp()
                }
            )
        }
    }
}
