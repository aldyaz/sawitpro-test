package com.aldyaz.sawitpro.data.repository

import android.util.Log
import com.aldyaz.sawitpro.base.ResultState
import com.aldyaz.sawitpro.data.model.TicketDto
import com.aldyaz.sawitpro.domain.model.FilterType
import com.aldyaz.sawitpro.domain.model.SortType
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import javax.inject.Inject
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

class FirestoreRepositoryImpl @Inject constructor(
    private val firestore: FirebaseFirestore
) : FirestoreRepository {

    override suspend fun getTickets(
        sortBy: SortType,
        filterType: FilterType?
    ): ResultState<List<TicketDto>> = suspendCoroutine { cont ->
        var ref = firestore.collection(TICKET)
            .orderBy(sortBy.fieldName)

        if (filterType != null) {
            if (filterType.fromDate > 0) {
                ref = ref.whereGreaterThanOrEqualTo("dateTime", filterType.fromDate)
            }

            if (filterType.toDate > 0) {
                ref = ref.whereLessThanOrEqualTo("dateTime", filterType.fromDate)
            }

            if (filterType.driverName.isNotEmpty()) {
                ref = ref.whereEqualTo("driverName", filterType.driverName)
            }

            if (filterType.licenseNumber.isNotEmpty()) {
                ref = ref.whereEqualTo("licenseNumber", filterType.licenseNumber)
            }
        }

        ref.get()
            .addOnCompleteListener {
                it.exception?.printStackTrace()
            }
            .addOnSuccessListener { snapshot ->
                val items = snapshot.map { document ->
                    TicketDto(
                        id = document.id,
                        dateTime = document.getLong("dateTime"),
                        licenseNumber = document.getString("licenseNumber"),
                        driverName = document.getString("driverName"),
                        inboundWeight = document.getLong("inboundWeight")?.toInt(),
                        outboundWeight = document.getLong("outboundWeight")?.toInt(),
                        netWeight = document.getLong("netWeight")?.toInt()
                    )
                }
                cont.resume(ResultState.Success(items))
            }
            .addOnFailureListener { exception ->
                exception.printStackTrace()
                cont.resume(ResultState.Error(exception))
            }
    }

    override suspend fun getTicket(id: String): ResultState<TicketDto> = suspendCoroutine { cont ->
        firestore.collection(TICKET)
            .document(id)
            .get()
            .addOnFailureListener { exception ->
                cont.resume(ResultState.Error(exception))
            }
            .addOnSuccessListener { snapshot ->
                val data = snapshot.data
                data?.also {
                    cont.resume(
                        ResultState.Success(
                            TicketDto(
                                id = snapshot.id,
                                dateTime = it["dateTime"] as? Long,
                                licenseNumber = it["licenseNumber"]?.toString(),
                                driverName = it["driverName"]?.toString(),
                                inboundWeight = it["inboundWeight"] as? Int,
                                outboundWeight = it["outboundWeight"] as? Int,
                                netWeight = it["netWeight"] as? Int
                            )
                        )
                    )
                }
            }
    }

    override suspend fun insertTicket(ticket: TicketDto): ResultState<Unit> =
        suspendCoroutine { cont ->
            val payload = ticket.getMap()
            val ref = firestore.collection(TICKET)

            val docRef = if (!ticket.id.isNullOrEmpty()) {
                ref.document(ticket.id)
            } else {
                ref.document()
            }

            docRef.set(payload)
                .addOnFailureListener { exception ->
                    cont.resume(ResultState.Error(exception))
                }
                .addOnCompleteListener {
                    cont.resume(ResultState.Success(Unit))
                }
        }

    companion object {

        private const val TICKET = "ticket"
    }
}