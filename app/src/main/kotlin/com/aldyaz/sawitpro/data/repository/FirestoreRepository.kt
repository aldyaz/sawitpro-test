package com.aldyaz.sawitpro.data.repository

import com.aldyaz.sawitpro.base.ResultState
import com.aldyaz.sawitpro.data.model.TicketDto
import com.aldyaz.sawitpro.domain.model.FilterType
import com.aldyaz.sawitpro.domain.model.SortType

interface FirestoreRepository {

    suspend fun getTickets(
        sortBy: SortType = SortType.DATE_TIME,
        filterType: FilterType?
    ): ResultState<List<TicketDto>>

    suspend fun getTicket(id: String): ResultState<TicketDto>

    suspend fun insertTicket(ticket: TicketDto): ResultState<Unit>

}