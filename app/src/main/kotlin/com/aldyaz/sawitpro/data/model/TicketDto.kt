package com.aldyaz.sawitpro.data.model

import com.google.firebase.database.Exclude

data class TicketDto(
    val id: String? = null,
    val dateTime: Long? = null,
    val licenseNumber: String? = null,
    val driverName: String? = null,
    val inboundWeight: Int? = null,
    val outboundWeight: Int? = null,
    val netWeight: Int? = null
) {

    @Exclude
    fun getMap(): HashMap<String, Any?> {
        return hashMapOf(
            "dateTime" to dateTime,
            "licenseNumber" to licenseNumber,
            "driverName" to driverName,
            "inboundWeight" to inboundWeight,
            "outboundWeight" to outboundWeight,
            "netWeight" to netWeight
        )
    }

    fun setMap(map: Map<String, Any>): TicketDto {
        return TicketDto(
            dateTime = map["dateTime"] as? Long,
            licenseNumber = map["licenseNumber"]?.toString(),
            driverName = map["driverName"]?.toString(),
            inboundWeight = map["inboundWeight"] as? Int,
            outboundWeight = map["outboundWeight"] as? Int,
            netWeight = map["netWeight"] as? Int
        )
    }
}