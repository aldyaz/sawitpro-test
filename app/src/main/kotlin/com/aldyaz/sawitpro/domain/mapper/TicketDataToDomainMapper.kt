package com.aldyaz.sawitpro.domain.mapper

import com.aldyaz.sawitpro.data.model.TicketDto
import com.aldyaz.sawitpro.domain.model.Ticket

class TicketDataToDomainMapper : (TicketDto) -> Ticket {

    override fun invoke(
        p1: TicketDto
    ): Ticket = Ticket(
        id = p1.id.orEmpty(),
        dateTime = p1.dateTime ?: 0L,
        licenseNumber = p1.licenseNumber.orEmpty(),
        driverName = p1.driverName.orEmpty(),
        inboundWeight = p1.inboundWeight ?: 0,
        outboundWeight = p1.outboundWeight ?: 0,
        netWeight = p1.netWeight ?: 0
    )
}