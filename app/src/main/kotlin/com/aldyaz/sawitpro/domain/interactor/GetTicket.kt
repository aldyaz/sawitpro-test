package com.aldyaz.sawitpro.domain.interactor

import com.aldyaz.sawitpro.base.ResultState
import com.aldyaz.sawitpro.domain.model.Ticket

interface GetTicket {

    suspend operator fun invoke(id: String): ResultState<Ticket>

}