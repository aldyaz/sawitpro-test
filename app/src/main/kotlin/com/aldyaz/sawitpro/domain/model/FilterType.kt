package com.aldyaz.sawitpro.domain.model

data class FilterType(
    val fromDate: Long = 0L,
    val toDate: Long = 0L,
    val driverName: String = "",
    val licenseNumber: String = ""
)