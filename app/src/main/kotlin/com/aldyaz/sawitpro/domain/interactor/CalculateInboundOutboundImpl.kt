package com.aldyaz.sawitpro.domain.interactor

import com.aldyaz.sawitpro.base.ResultState
import com.aldyaz.sawitpro.domain.model.UiState
import javax.inject.Inject
import kotlin.math.abs

class CalculateInboundOutboundImpl @Inject constructor() : CalculateInboundOutbound {

    override suspend fun invoke(inboundWeight: Int, outboundWeight: Int): ResultState<Int> {
        return when {
            inboundWeight <= outboundWeight -> ResultState.Error(
                Exception("Outbound should less than Inbound")
            )

            else -> ResultState.Success(abs(inboundWeight - outboundWeight))
        }
    }
}