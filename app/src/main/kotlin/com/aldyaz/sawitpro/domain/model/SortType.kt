package com.aldyaz.sawitpro.domain.model

enum class SortType(
    val fieldName: String
) {
    DATE_TIME("dateTime"),
    DRIVER_NAME("driverName"),
    LICENSE_NUMBER("licenseNumber")
}