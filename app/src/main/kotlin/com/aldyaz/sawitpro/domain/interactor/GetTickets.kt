package com.aldyaz.sawitpro.domain.interactor

import com.aldyaz.sawitpro.base.ResultState
import com.aldyaz.sawitpro.domain.model.FilterType
import com.aldyaz.sawitpro.domain.model.SortType
import com.aldyaz.sawitpro.domain.model.Ticket

interface GetTickets {

    suspend operator fun invoke(
        sortType: SortType,
        filterType: FilterType?
    ): ResultState<List<Ticket>>

}