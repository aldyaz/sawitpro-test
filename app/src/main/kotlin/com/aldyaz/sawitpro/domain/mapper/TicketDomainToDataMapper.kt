package com.aldyaz.sawitpro.domain.mapper

import com.aldyaz.sawitpro.data.model.TicketDto
import com.aldyaz.sawitpro.domain.model.Ticket

class TicketDomainToDataMapper : (Ticket) -> TicketDto {

    override fun invoke(
        p1: Ticket
    ): TicketDto = TicketDto(
        id = p1.id,
        dateTime = p1.dateTime,
        licenseNumber = p1.licenseNumber,
        driverName = p1.driverName,
        inboundWeight = p1.inboundWeight,
        outboundWeight = p1.outboundWeight,
        netWeight = p1.netWeight
    )
}