package com.aldyaz.sawitpro.domain.model

data class Ticket(
    val id: String,
    val dateTime: Long,
    val licenseNumber: String,
    val driverName: String,
    val inboundWeight: Int,
    val outboundWeight: Int,
    val netWeight: Int
)