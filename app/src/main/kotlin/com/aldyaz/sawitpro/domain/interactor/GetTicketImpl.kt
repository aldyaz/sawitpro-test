package com.aldyaz.sawitpro.domain.interactor

import com.aldyaz.sawitpro.base.ResultState
import com.aldyaz.sawitpro.data.repository.FirestoreRepository
import com.aldyaz.sawitpro.domain.mapper.TicketDataToDomainMapper
import com.aldyaz.sawitpro.domain.model.Ticket
import javax.inject.Inject

class GetTicketImpl @Inject constructor(
    private val firestoreRepository: FirestoreRepository,
    private val dataToDomainMapper: TicketDataToDomainMapper
) : GetTicket {

    override suspend fun invoke(id: String): ResultState<Ticket> {
        return when (val result = firestoreRepository.getTicket(id)) {
            is ResultState.Error -> result
            is ResultState.Success -> ResultState.Success(dataToDomainMapper(result.data))
        }
    }
}