package com.aldyaz.sawitpro.domain.interactor

import com.aldyaz.sawitpro.base.ResultState

interface CalculateInboundOutbound {

    suspend operator fun invoke(
        inboundWeight: Int,
        outboundWeight: Int
    ): ResultState<Int>

}