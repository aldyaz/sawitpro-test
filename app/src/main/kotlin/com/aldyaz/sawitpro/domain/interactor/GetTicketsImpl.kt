package com.aldyaz.sawitpro.domain.interactor

import com.aldyaz.sawitpro.base.ResultState
import com.aldyaz.sawitpro.data.repository.FirestoreRepository
import com.aldyaz.sawitpro.domain.mapper.TicketDataToDomainMapper
import com.aldyaz.sawitpro.domain.model.FilterType
import com.aldyaz.sawitpro.domain.model.SortType
import com.aldyaz.sawitpro.domain.model.Ticket
import javax.inject.Inject

class GetTicketsImpl @Inject constructor(
    private val firestoreRepository: FirestoreRepository,
    private val ticketDataToDomainMapper: TicketDataToDomainMapper
) : GetTickets {

    override suspend fun invoke(
        sortType: SortType,
        filterType: FilterType?
    ): ResultState<List<Ticket>> {
        return when (val result = firestoreRepository.getTickets(
            sortBy = sortType,
            filterType = filterType
        )) {
            is ResultState.Error -> result
            is ResultState.Success -> ResultState.Success(
                result.data.map(ticketDataToDomainMapper)
            )
        }
    }
}