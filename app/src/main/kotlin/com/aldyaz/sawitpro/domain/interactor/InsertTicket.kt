package com.aldyaz.sawitpro.domain.interactor

import com.aldyaz.sawitpro.base.ResultState
import com.aldyaz.sawitpro.domain.model.Ticket

interface InsertTicket {

    suspend operator fun invoke(payload: Ticket): ResultState<Unit>

}