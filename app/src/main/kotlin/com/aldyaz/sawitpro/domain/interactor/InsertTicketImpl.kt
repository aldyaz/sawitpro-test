package com.aldyaz.sawitpro.domain.interactor

import com.aldyaz.sawitpro.base.ResultState
import com.aldyaz.sawitpro.data.repository.FirestoreRepository
import com.aldyaz.sawitpro.domain.mapper.TicketDomainToDataMapper
import com.aldyaz.sawitpro.domain.model.Ticket
import com.aldyaz.sawitpro.domain.model.UiState
import javax.inject.Inject

class InsertTicketImpl @Inject constructor(
    private val firestoreRepository: FirestoreRepository,
    private val domainToDataMapper: TicketDomainToDataMapper
) : InsertTicket {

    override suspend fun invoke(payload: Ticket): ResultState<Unit> {
        return when (val result = firestoreRepository.insertTicket(domainToDataMapper(payload))) {
            is ResultState.Error -> ResultState.Error(result.throwable)
            is ResultState.Success -> ResultState.Success(result.data)
        }
    }
}