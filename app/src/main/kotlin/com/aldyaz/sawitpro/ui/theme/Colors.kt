package com.aldyaz.sawitpro.ui.theme

import androidx.compose.material3.lightColorScheme
import androidx.compose.ui.graphics.Color

val Blue500 = Color(0xFF216BFF)

val LightColorScheme = lightColorScheme()