package com.aldyaz.sawitpro.ui.theme

import androidx.compose.material3.Typography
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.sp
import com.aldyaz.sawitpro.R

val AvenirNext = FontFamily(
    Font(
        resId = R.font.metropolis_regular,
        weight = FontWeight.Normal
    ),
    Font(
        resId = R.font.metropolis_extra_bold,
        weight = FontWeight.ExtraBold
    ),
    Font(
        resId = R.font.metropolis_bold,
        weight = FontWeight.Bold
    ),
    Font(
        resId = R.font.metropolis_semi_bold,
        weight = FontWeight.SemiBold
    )
)
