package com.aldyaz.sawitpro.base

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers

interface DispatcherProvider {

    val io: CoroutineDispatcher
        get() = Dispatchers.IO

}