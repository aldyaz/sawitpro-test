package com.aldyaz.sawitpro.base

sealed class Screen(
    route: String
) {
    
    object TicketList : Screen(Route.TICKET_LIST)

    object InputTicket : Screen(Route.INPUT_TICKET) {
        private val BASE_ROUTE = "input_ticket"

        fun pattern(id: String = ""): String {
            return if (id.isNotEmpty()) {
                "$BASE_ROUTE/$id"
            } else Route.INPUT_TICKET
        }
    }

}

object Route {
    const val TICKET_LIST = "ticket_list"
    const val INPUT_TICKET = "input_ticket/{id}"
}
