package com.aldyaz.sawitpro.base

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

abstract class BaseViewModel<INTENT> : ViewModel() {

    abstract fun onDispatchIntent(intent: INTENT)

    protected fun <T> MutableLiveData<T>.updateState(
        state: T.() -> T
    ) {
        value = value?.let {
            state.invoke(it)
        }
    }

    protected fun <T> MutableLiveData<T>.postUpdateState(
        state: T.() -> T
    ) {
        postValue(
            value?.let {
                state.invoke(it)
            }
        )
    }

}